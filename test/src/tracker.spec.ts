import { random } from "faker";
import { dbQuery, syncDB } from "./utils/db"
import { get } from "request-promise-native";
import { json } from "body-parser";
import { expect } from "chai";

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;
describe('Tracker server', function () {
    this.timeout(30000);
    before(function (done) {
        setTimeout(done, 15000)
    })
    beforeEach(async function () {
        await syncDB;
        await dbQuery({ type: "remove", table: "track_events" });

        this.tracks = (await dbQuery({
            type: "insert",
            table: "track_events",
            values: {
                "rider_id": 4,
                "north": random.number({ min: 1, max: 30 }),
                "east": random.number({ min: 1, max: 30 }),
                "west": random.number({ min: 1, max: 30 }),
                "south": random.number({ min: 1, max: 30 }),
                "createdAt": new Date(),
                "updatedAt": new Date(),
            },
            returning: [
                "rider_id",
                "north",
                "east",
                "west",
                "south",
                "createdAt"
            ]
        }))[0][0]
    })

    describe('Movement', function () {
        it('harusnya memberikan data suatu driver', async function () {
            const response = await get(
                `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`,
                { json: true }
            );
            expect(response.ok).to.be.true;
            const obj = {
                time: this.tracks.createdAt.toISOString(),
                east: this.tracks.east,
                west: this.tracks.west,
                north: this.tracks.north,
                south: this.tracks.south
            }
            expect(response.logs[0]).to.be.deep.equal(obj);
        })
    })
})